package hr.lmandic.recyclerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements NameClickListener{
    private RecyclerView recycler;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();
        setupRecyclerData();
    }

    private void setupRecyclerData() {
        List<String> data=new ArrayList<>();
        data.add("Ana");
        data.add("Ana");
        data.add("Ana");

        adapter.addData(data);
    }

    private void setupRecycler() {
        recycler=findViewById(R.id.recyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter=new RecyclerAdapter(this);
        recycler.setAdapter(adapter);
    }

    public void addCell(View view){
        Log.d("LauraM","main prosao");
        adapter.addNewCell("Ptra",1);
    }
    public void removeCell(View view){
        adapter.removeCell(0);
    }

    @Override
    public void onNameClick(int position) {
        Toast.makeText(this,"position"+ position,Toast.LENGTH_SHORT).show();
    }
}