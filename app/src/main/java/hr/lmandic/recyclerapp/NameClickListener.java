package hr.lmandic.recyclerapp;

public interface NameClickListener {
    void onNameClick(int position);
}
