package hr.lmandic.recyclerapp;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class NameViewHolder extends ViewHolder implements View.OnClickListener {
    private TextView liTextView;
    private NameClickListener clickListener;
    public NameViewHolder(@NonNull View itemView,NameClickListener listener) {
        super(itemView);
        this.clickListener=listener;
        liTextView = itemView.findViewById(R.id.liTextView);
        itemView.setOnClickListener(this);
    }
    public void setName(String name){
        liTextView.setText(name);
    }

    @Override
    public void onClick(View v) {
    clickListener.onNameClick(getAdapterPosition());
    }
}